<?php declare(strict_types=1);

namespace EdmondsCommerce\DoctrineAssert\Exceptions;

use PHPUnit\Runner\Exception;

class DoctrineAssertException extends Exception
{
    
}